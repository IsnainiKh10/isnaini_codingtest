/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Home from './src/component/section/HomeComponent';
import Routing from './src/navigation/Routing';

AppRegistry.registerComponent(appName, () => Routing);
