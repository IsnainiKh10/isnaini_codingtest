import React from 'react';
import {
  Text,
  FlatList,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {Colors} from '../../styles';
import {TextBold, TextMedium, TextRegular} from '../global';
import {ScrollView} from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';

const DetailContactComponent = ({navigation, dataDetail}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#DBE8DA'}}>
      <ScrollView>
        {/* <ImageBackground
          source={require('../../asset/image/wallpaper.jpg')}
          resizeMode="cover"
          style={{
            flex: 1,
            paddingTop: 20,
            paddingBottom: 90,
            alignItems: 'center',
            alignContent: 'center',
          }}>
          
        </ImageBackground> */}
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            paddingVertical: 5,
            marginTop: 0,
            borderColor: Colors.BORDERGREY,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: '95%',
            }}>
            <View
              style={{
                justifyContent: 'center',
                // alignSelf: 'center',
                alignContent: 'center',
              }}>
              <TouchableOpacity onPress={() => navigation.pop()}>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '72%',
                    marginTop: 20,
                    justifyContent: 'space-between',
                  }}>
                  <Ionicons
                    name="arrow-back-outline"
                    size={25}
                    color={Colors.DEEPBLUE}
                  />
                  <TextBold
                    text={'Detail Contact'}
                    color={Colors.DEEPBLUE}
                    size={20}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={{width: 30, height: 30}}></View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              borderWidth: 1,
              borderColor: Colors.GREY,
              marginBottom: 5,
              borderRadius: 20,
              marginTop: 20,
              backgroundColor: Colors.WHITE,
            }}>
            <Image
              source={{uri: dataDetail.photo}}
              style={{
                width: 130,
                height: 130,

                borderRadius: 20,
                borderWidth: 1,
                borderColor: Colors.WHITE,
                justifyContent: 'flex-start',
                alignSelf: 'flex-start',
              }}
            />
            <View
              style={{
                flexDirection: 'column',
                paddingLeft: 20,
                paddingVertical: 20,

                // justifyContent: 'center',
                // alignSelf: 'center',
              }}>
              <View style={{flexDirection: 'row'}}>
                <TextMedium
                  size={20}
                  color={Colors.DEEPBLUE}
                  text={dataDetail?.firstName + ' '}
                />
                <TextMedium
                  size={20}
                  color={Colors.DEEPBLUE}
                  text={dataDetail?.lastName}
                />
              </View>
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                {/* <Entypo name="person" size={20} /> */}
                <TextRegular
                  size={12}
                  color={Colors.BLACK}
                  text={dataDetail?.age + ' years old'}
                />
              </View>

              <View
                style={{
                  // marginTop: 20,
                  width: 100,
                  height: 30,
                  backgroundColor: Colors.WHITE,
                  borderRadius: 4,
                  borderColor: Colors.BORDERGREY,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderWidth: 1,
                }}>
                <TextMedium text={'Developer'} color={Colors.DEEPBLUE} />
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            width: '90%',
            backgroundColor: Colors.WHITE,
            alignSelf: 'center',
            borderRadius: 20,
            padding: 10,
          }}>
          <View style={styles.boxplace_mapsdetail}>
            <Ionicons
              name="md-location-sharp"
              size={30}
              color={'#BB2427'}
              //marginLeft={20}
              style={{marginRight: 10}}
            />
            <TextRegular
              text={
                'Jl babarsari tambak bayan III no. 5d Slemanfa sekitarnya df'
              }
              size={20}
              style={styles.place_alamat_txt}
            />
          </View>

          <TextBold
            text={'Phone Number'}
            size={16}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <TextRegular
            text={'+6282241615847S'}
            size={14}
            color={Colors.BLACK}
            style={{marginTop: 10, marginLeft: 5}}
          />

          <TextBold
            text={'Email'}
            size={16}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <TextRegular
            text={'IsnainiKhaira@gmail.com'}
            size={14}
            color={Colors.BLACK}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <TextBold
            text={'Date Birth'}
            size={16}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <TextRegular
            text={'Selasa, 10 April 2001'}
            size={14}
            color={Colors.BLACK}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <TextBold
            text={'Hobby'}
            size={16}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <TextRegular
            text={'Drakoran'}
            size={14}
            color={Colors.BLACK}
            style={{marginTop: 10, marginLeft: 5}}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  contactItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
    marginTop: 10,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 12,
  },
  contactInfo: {
    flex: 1,
    flexDirection: 'row',
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  email: {
    fontSize: 14,
    color: '#888',
  },
  place_name_txt: {
    fontWeight: '700',
    fontSize: 18,
    lineHeight: 17,
    color: '#201F26',
    height: 34,
    width: 225,
  },
  placerating_ic: {
    width: 50.72,
    height: 7.76,
  },
  place_alamat_txt: {
    height: 37,
    color: Colors.BLACK,
    width: 216,
    fontFamily: 'Montserrat',
    fontWeight: '400',
    fontSize: 12,
    lineHeight: 17,
  },

  place_mapview_txt: {
    fontFamily: 'montserrat',
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 17,
    letterSpacing: 0.57,
    color: '#3471CD',
  },

  place_jam_txt: {
    height: 15,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 17,
    letterSpacing: 0.257,
    color: '#343434',
    marginLeft: 24,
  },

  boxplace_mapsdetail: {
    flexDirection: 'row',
    alignItems: 'center',
    //alignSelf: 'center',
    alignContent: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 13,
  },

  boxplace_2: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'baseline',
    marginTop: 15,
    backgroundColor: '#FFFFFF',
  },
  place_openclose_txt: {
    //height: 15,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 17,
    color: '#EA3D3D',
    //   marginLeft: 10,
  },

  place_deskname_txt: {
    height: 20,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 17,
    letterSpacing: 0.257143,
    color: '#201F26',
  },
  place_deskripsi: {
    height: 134,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: 0.257143,
    color: '#595959',
    marginTop: 2,
    //marginLeft: 2,
  },

  place_rangeBiaya2_txt: {
    height: 20,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 17,
    letterSpacing: 0.257143,
    color: '#8d8d8d',
    marginTop: 6,
  },

  place_rangeBiaya_txt: {
    height: 20,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 17,
    letterSpacing: 0.257143,
    color: '#201F26',
    marginTop: 23,
  },

  LineView: {
    backgroundColor: '#959595',
    width: '100%',
    height: 2,
    //alignItems: 'center',
    //marginTop: 15,
  },
});
export default DetailContactComponent;
