import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  CheckBox,
  StyleSheet,
  Dimensions,
  Alert,
  RefreshControl,
  Modal,
  ActivityIndicator,
} from 'react-native';
//import CheckBox from '@react-native-community/checkbox';

// import AuthNavigation from '../AuthNavigation';

import Entypo from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/AntDesign';
import apiProvider from '../../utils/service/apiProvider';
import {FlatList, ScrollView} from 'react-native-gesture-handler';
import {
  TextRegular,
  TextBold,
  TextMedium,
  Header,
  InputText,
} from '../global/index';
import {Colors} from '../../styles';
import {useIsFocused} from '@react-navigation/native';
import DetailDataModal from '../modal/Detail';
import ModalAddData from '../modal/ModalAddData';

const HomeComponent = ({navigation, listContact, OnDelete}) => {
  // const [listContact, setListContact] = useState('');

  const [addDataModal, setAddDataModal] = useState(false);
  const [listDataModal, setListDataModal] = useState(false);
  const [refreshing, setRefreshing] = React.useState(false);
  const isFocused = useIsFocused();
  const [loading, setLoading] = useState(false);
  const [modalBottom, setModalBottom] = useState(false);
  const item = listContact;

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getDataMobil();
    setTimeout(() => {
      setRefreshing(false);
    }, 500);
  }, [isFocused]);

  const RenderListItem = ({item}) => (
    <TouchableOpacity
      onPress={() => navigation.navigate('DetailContact', item)}
      style={{
        width: '90%',
        alignSelf: 'center',
        marginTop: 15,
        borderRadius: 4,
      }}>
      {/* {console.log('ini log', item)} */}
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: Colors.WHITE,
          height: 100,
          borderWidth: 1,
          borderColor: Colors.GREY,
          padding: 5,
          borderRadius: 10,
          paddingHorizontal: 10,
          justifyContent: 'space-between',
          alignContent: 'flex-start',
          alignItems: 'flex-start',
        }}>
        <View
          style={{
            width: '25%',
            height: '100%',
            // padding: 1,
          }}>
          <Image
            style={{
              width: '100%',
              height: '100%',
              borderRadius: 10,
              // borderColor: Colors.WHITE,
            }}
            source={{uri: item?.photo}}></Image>
        </View>

        <View
          style={{
            flexDirection: 'column',
            paddingLeft: 20,
            paddingVertical: 20,
            // justifyContent: 'center',
            // alignSelf: 'center',

            marginLeft: 10,
          }}>
          <View style={{flexDirection: 'row'}}>
            <TextMedium
              size={20}
              color={Colors.DEEPBLUE}
              text={item?.firstName + ' '}
            />
            <TextMedium
              size={20}
              color={Colors.DEEPBLUE}
              text={item?.lastName}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            {/* <Entypo name="person" size={20} /> */}
            <TextBold
              size={12}
              color={Colors.BLACK}
              text={item?.age + ' years old'}
            />
          </View>
        </View>
        <TextBold text={''} />
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('EditContact', item);
          }}>
          <View
            style={{
              backgroundColor: Colors.PRIMARY,
              height: 30,
              width: 30,
              marginLeft: 10,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 40,
              marginVertical: 30,
            }}>
            <Image
              source={require('../../asset/icon/edit.png')}
              style={{
                width: 20,
                height: 20,
                tintColor: Colors.WHITE,
                marginLeft: 4,
              }}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            OnDelete(item);
          }}>
          <View
            style={{
              backgroundColor: Colors.RED,
              height: 30,
              width: 30,
              alignItems: 'center',
              justifyContent: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 40,
              marginVertical: 30,
            }}>
            <Image
              source={require('../../asset/icon/delete.png')}
              style={{
                width: 20,
                height: 20,
                tintColor: Colors.WHITE,
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={{backgroundColor: '#DBE8DA'}}>
      <Modal animationType="fade" transparent={true} visible={loading}>
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
            alignContent: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              padding: 20,
              borderRadius: 18,
              backgroundColor: '#FFFFFF',
            }}>
            <ActivityIndicator size="large" color="#00FF00" />
          </View>
        </View>
      </Modal>
      <View style={styles.header}>
        <TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              paddingHorizontal: 10,
            }}>
            <TextMedium text={'LIST USERS'} size={18} color={Colors.DEEPBLUE} />
          </View>
        </TouchableOpacity>
      </View>
      <View
        style={{
          alignItems: 'flex-end',
          justifyContent: 'flex-end',

          padding: 16,
        }}>
        <TouchableOpacity
          style={{
            width: 50,
            height: 50,
            // justifyContent: 'flex-end',
            borderRadius: 40,
            backgroundColor: 'red',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => setModalBottom(true)}>
          <Icon name="plus" size={20} color="#fff" />
        </TouchableOpacity>
        <ModalAddData
          show={modalBottom}
          onClose={() => setModalBottom(false)}></ModalAddData>
      </View>
      <FlatList
        data={listContact}
        renderItem={({item}) => <RenderListItem item={item} />}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      />

      {/* <DetailDataModal
        show={listDataModal}
        onClose={() => setListDataModal(false)}
        title="Konfrimasi Data"
        item={item}
      /> */}
    </View>
  );
};
export default HomeComponent;

const styles = StyleSheet.create({
  button: {
    marginRight: 20,
    marginLeft: 20,
    backgroundColor: Colors.RED,
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'flex-end',
    alignItems: 'center',
    alignContent: 'flex-end',
    marginTop: 20,
  },
  box: {
    width: '87%',
    height: 50,
    paddingHorizontal: 10,
    alignItems: 'center',
    backgroundColor: 'aqua',
    // borderRadius: 10,
    alignSelf: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    marginVertical: 5,
    // backgroundColor:Colors.AQUA
  },
  header: {
    width: '100%',
    height: 60,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  btnFloating: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
