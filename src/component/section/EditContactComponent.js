import React from 'react';
import {
  Text,
  FlatList,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {Colors} from '../../styles';
import {TextBold, TextMedium, TextRegular} from '../global';
import {ScrollView} from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';

const EditContactComponent = ({
  navigation,
  dataDetail,
  onUpdate,
  photo,
  age,
  lastName,
  firstName,
  setPhoto,
  setAge,
  setFirstName,
  setLastName,
}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#DBE8DA'}}>
      <ScrollView>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            borderRadius: 20,
            paddingVertical: 5,
            marginTop: 0,
            borderColor: Colors.BORDERGREY,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: '95%',
            }}>
            <View
              style={{
                justifyContent: 'center',
                // alignSelf: 'center',
                alignContent: 'center',
              }}>
              <TouchableOpacity onPress={() => navigation.pop()}>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '72%',
                    marginTop: 20,
                    justifyContent: 'space-between',
                  }}>
                  <Ionicons
                    name="arrow-back-outline"
                    size={25}
                    color={Colors.DEEPBLUE}
                  />
                  <TextBold
                    text={'Edit Contact'}
                    color={Colors.DEEPBLUE}
                    size={20}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={{width: 30, height: 30}}></View>
          </View>
        </View>
        <View
          style={{
            width: '90%',
            backgroundColor: Colors.WHITE,
            alignSelf: 'center',
            borderRadius: 20,
            padding: 10,
            marginTop: 30,
          }}>
          <TextBold
            text={'First Name'}
            size={16}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <TextInput
            value={firstName}
            onChangeText={text => setFirstName(text)}
            style={{
              backgroundColor: Colors.GREY,
              borderRadius: 10,
              marginTop: 10,
            }}></TextInput>

          <TextBold
            text={'Last Name'}
            size={16}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <TextInput
            value={lastName}
            onChangeText={text => setLastName(text)}
            style={{
              backgroundColor: Colors.GREY,
              borderRadius: 10,
              marginTop: 10,
            }}></TextInput>
          <TextBold
            text={'Age'}
            size={16}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <TextInput
            value={age}
            onChangeText={text => setAge(text)}
            keyboardType="numeric"
            style={{
              backgroundColor: Colors.GREY,
              borderRadius: 10,
              marginTop: 10,
            }}></TextInput>
          <TextBold
            text={'Image Url'}
            size={16}
            color={Colors.DEEPBLUE}
            style={{marginTop: 10, marginLeft: 5}}
          />
          <View style={{borderColor: Colors.GREY}}></View>
          <TextInput
            value={photo}
            onChangeText={text => setPhoto(text)}
            style={{
              backgroundColor: Colors.GREY,
              borderRadius: 10,
              marginTop: 10,
            }}></TextInput>
          <TouchableOpacity
            style={{
              width: '50%',
              padding: 10,
              backgroundColor: Colors.BACKGROUND,
              marginTop: 50,
              alignSelf: 'center',
              borderRadius: 40,
              alignItems: 'center',
              alignContent: 'center',
            }}
            onPress={() => {
              onUpdate();
              navigation.navigate('Home');
            }}>
            <TextBold text={'SIMPAN'} color={Colors.BLACK} size={16} />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  contactItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
    marginTop: 10,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 12,
  },
  contactInfo: {
    flex: 1,
    flexDirection: 'row',
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  email: {
    fontSize: 14,
    color: '#888',
  },
});
export default EditContactComponent;
