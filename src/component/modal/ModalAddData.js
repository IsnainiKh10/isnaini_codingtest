import React, {useState} from 'react';
import {
  View,
  Modal,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import {Colors} from '../../styles';
import Icon from 'react-native-vector-icons/AntDesign';
import {TextBold} from '../global';

const ModalAddData = ({show, title, onClose}) => {
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View
          style={{
            width: '80%',
            borderRadius: 10,
            backgroundColor: Colors.WHITE,
            paddingBottom: 20,
            borderWidth: 2,
            borderColor: Colors.BACKGROUND,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignContent: 'center',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingHorizontal: 10,
              marginTop: 10,
            }}>
            <TextBold text={'Tambah Data'} size={20} color={Colors.DEEPBLUE} />
            <TouchableOpacity
              onPress={onClose}
              style={{
                width: 30,
                height: 30,
                marginTop: 5,
                alignSelf: 'flex-end',
              }}>
              <Image
                source={require('../../asset/icon/close.png')}
                style={{width: 30, height: 30, tintColor: Colors.BACKGROUND}}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: '100%',
              height: 2,

              marginTop: 10,
              marginBottom: 10,
            }}></View>

          <TextInput style={styles.input} placeholder="First Name" />
          <TextInput style={styles.input} placeholder="Last Name" />
          <TextInput
            style={styles.input}
            placeholder="Age"
            keyboardType="numeric"
          />
          <TextInput style={styles.input} placeholder="Photo Link" />

          <TouchableOpacity
            style={{
              width: '90%',
              paddingVertical: 10,
              backgroundColor: Colors.BACKGROUND,
              alignSelf: 'center',
              borderRadius: 10,
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <TextBold text={'SIMPAN'} color={Colors.BLACK} />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: Colors.BACKGROUND,
    borderBottomWidth: 1,
  },
  input: {
    borderWidth: 1,
    borderColor: Colors.BACKGROUND,
    borderRadius: 8,
    padding: 10,
    marginBottom: 15,
    width: '90%',
    alignSelf: 'center',
  },
  body: {
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  option: {
    paddingHorizontal: 16,
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  filter: {
    backgroundColor: '#E4E7EB',
  },
});

export default ModalAddData;
