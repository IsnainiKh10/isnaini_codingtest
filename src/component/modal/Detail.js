import React from 'react';
import {
  View,
  Modal,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import {TextBold, TextRegular} from '../global/Text';
import {Colors} from '../../styles';
import Icon from 'react-native-vector-icons/AntDesign';

const DetailDataModal = ({show, onClose, item}) => {
  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.modalContent}>
          <Image
            source={require('../../asset/icon/profile.png')}
            style={styles.profileImage}
          />
          <Text style={styles.name}>nama</Text>
          <Text style={styles.age}>age</Text>
          <TouchableOpacity style={styles.closeButton} onPress={onClose}>
            <Text style={styles.closeButtonText}>Close</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
    // <Modal
    //   visible={show}
    //   animationType="slide"
    //   transparent={true}
    //   onRequestClose={onClose}>
    //   <View style={styles.modalContainer}>
    //     <View style={styles.modalContent}>
    //       <Image
    //         source={require('../../asset/icon/profile.png')}
    //         style={styles.profileImage}
    //       />
    //       <Text style={styles.name}>nama</Text>
    //       <Text style={styles.age}>age</Text>
    //       <TouchableOpacity style={styles.closeButton} onPress={onClose}>
    //         <Text style={styles.closeButtonText}>Close</Text>
    //       </TouchableOpacity>
    //     </View>
    //   </View>
    // </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    paddingHorizontal: 16,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: '#E4E7EB',
    borderBottomWidth: 1,
  },
  body: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    width: '100%',
  },
  modalContainer: {
    // flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    alignItems: 'center',
    width: '100%',
  },
  profileImage: {
    width: 120,
    height: 120,
    borderRadius: 60,
    marginBottom: 10,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  age: {
    fontSize: 16,
    marginBottom: 20,
  },
  closeButton: {
    backgroundColor: '#2196F3',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  closeButtonText: {
    color: 'white',
    fontSize: 16,
  },
});

export default DetailDataModal;
