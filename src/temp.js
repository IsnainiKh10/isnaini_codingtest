import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const DetailPlace = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <Image
            source={require('./../asset/image/place_pic3.png')} //load atau panggil asset image dari local
            style={{
              width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
              height: 317,
            }}
          />
          <View
            style={{
              width: '100%',
              backgroundColor: '#fff',
              borderTopLeftRadius: 19,
              borderTopRightRadius: 19,
              paddingHorizontal: 20,
              paddingTop: 38,
              marginTop: -20,
            }}>
            <Text style={styles.place_name_txt}>Jack Repair Babarsari</Text>
            <Image
              source={require('../asset/icon/ratingbintang_ic.png')}
              style={styles.placerating_ic}></Image>

            <View style={styles.boxplace_mapsdetail}>
              <Ionicons
                name="md-location-sharp"
                size={24}
                color={'#BB2427'}
                //marginLeft={20}
                style={{marginRight:10}}
              />
              <Text style={styles.place_alamat_txt}>
                Jl babarsari tambak bayan III no. 5d Slemanfa sekitarnya df
                dsfnskdjf
              </Text>
              <Text style={styles.place_mapview_txt}>Lihat Maps</Text>
            </View>
            <View style={styles.boxplace_2}>
              <View
                style={{
                  width: 58,
                  //height: 21,
                  marginBottom:20,
                  backgroundColor: '#E64C3C33',
                  borderRadius: 10.5,
                  alignContent: 'center',
                  alignItems: 'center',
                  marginTop: 16,
                }}>
                <Text style={styles.place_openclose_txt}>TUTUP</Text>
              </View>
              <Text style={styles.place_jam_txt}>09.00-21.00</Text>
            </View>
            <View style={styles.LineView}></View>
            <Text style={styles.place_deskname_txt}>Deskripsi</Text>
            <Text style={styles.place_deskripsi_txt}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
              gravida mattis arcu interdum lectus egestas scelerisque. Blandit
              porttitor diam viverra amet nulla sodales aliquet est. Donec enim
              turpis rhoncus quis integer. Ullamcorper morbi donec tristique
              condimentum ornare imperdiet facilisi pretium molestie.
            </Text>
            <Text style={styles.place_rangeBiaya_txt}>Range Biaya</Text>
            <Text style={styles.place_rangeBiaya2_txt}>
              RP. 20.000 - 80.000
            </Text>

            <TouchableOpacity
              onPress={() => navigation.navigate('FormPemesanan')}
              style={{
                width: '100%',
                marginTop: 30,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Repair Disni
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};
export default DetailPlace;

const styles = StyleSheet.create({
  place_name_txt: {
    fontWeight: '700',
    fontSize: 18,
    lineHeight: 17,
    color: '#201F26',
    height: 34,
    width: 225,
  },
  placerating_ic: {
    width: 50.72,
    height: 7.76,
  },
  place_alamat_txt: {
    height: 37,
    color: '#979797',
    width: 216,
    fontFamily: 'Montserrat',
    fontWeight: '400',
    fontSize: 10,
    lineHeight: 17,
  },

  place_mapview_txt: {
    fontFamily: 'montserrat',
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 17,
    letterSpacing: 0.57,
    color: '#3471CD',
  },

  place_jam_txt: {
    height: 15,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 17,
    letterSpacing: 0.257,
    color: '#343434',
    marginLeft: 24,
  },

  boxplace_mapsdetail: {
    flexDirection: 'row',
    alignItems: 'center',
    //alignSelf: 'center',
    alignContent:'center',
    backgroundColor:'#FFFFFF',
    marginTop: 13,
  },

  boxplace_2: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'baseline',
    marginTop: 15,
    backgroundColor: '#FFFFFF',  
  },
  place_openclose_txt: {
    //height: 15,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 17,
    color: '#EA3D3D',
    //   marginLeft: 10,
  },

  place_deskname_txt: {
    height: 20,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 17,
    letterSpacing: 0.257143,
    color: '#201F26',
    marginTop: 23,
  },
  place_deskripsi: {
    height: 134,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: 0.257143,
    color: '#595959',
    marginTop: 10,
    //marginLeft: 2,
  },

  place_rangeBiaya2_txt: {
    height: 20,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 17,
    letterSpacing: 0.257143,
    color: '#8d8d8d',
    marginTop: 6,
  },

  place_rangeBiaya_txt: {
    height: 20,
    fontFamily: 'montserrat',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 17,
    letterSpacing: 0.257143,
    color: '#201F26',
    marginTop: 23,
  },

  LineView: {
    backgroundColor: '#959595',
    width: '100%',
    height: 2,
    //alignItems: 'center',
    //marginTop: 15,
  },
});
