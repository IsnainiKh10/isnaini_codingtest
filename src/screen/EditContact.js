import {useIsFocused} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';

import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import apiProvider from '../utils/service/apiProvider';
import EditContactComponent from '../component/section/EditContactComponent';

const EditContact = ({navigation, route}) => {
  const [item, setListContact] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [age, setAge] = useState('');
  const [photo, setPhoto] = useState('');
  const isFocused = useIsFocused();
  useEffect(() => {
    getDataContact();
  }, [isFocused]);
  const getDataContact = async () => {
    const response = await apiProvider.getDataDetailContact(route.params.id);
    if (response) {
      setListContact(response);
      setAge(response.data.age);
      setFirstName(response.data.firstName);
      setLastName(response.data.lastName);
      setPhoto(response.data.photo);
    }
  };
  const editDataContact = async () => {
    const body = {
      firstName: firstName,
      lastName: lastName,
      age: age,
      photo: photo,
    };
    const response = await apiProvider.EditDataContact(route.params.id, body);
    if (response.status == 201 || response.status == 200) {
      getDataContact();
      navigation.navigate('Home');
    }
  };

  return (
    <View style={{flex: 1}}>
      {item && photo && lastName && firstName && age ? (
        <EditContactComponent
          navigation={navigation}
          dataDetail={item.data}
          onUpdate={value => editDataContact(value)}
          photo={photo}
          age={age}
          lastName={lastName}
          firstName={firstName}
          setPhoto={setPhoto}
          setAge={setAge}
          setFirstName={setFirstName}
          setLastName={setLastName}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  contactItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 12,
  },
  contactInfo: {
    flex: 1,
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  email: {
    fontSize: 14,
    color: '#888',
  },
});
export default EditContact;
