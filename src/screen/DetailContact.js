import {useIsFocused} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';

import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import apiProvider from '../utils/service/apiProvider';
import DetailContactComponent from '../component/section/DetailContactComponent';

const DetailContact = ({navigation, route}) => {
  const [item, setListContact] = useState('');

  const isFocused = useIsFocused();
  useEffect(() => {
    getDataContact();
  }, [isFocused]);
  const getDataContact = async () => {
    const response = await apiProvider.getDataDetailContact(route.params.id);
    if (response) {
      return setListContact(response.data);
    }
  };

  return (
    <View style={{flex: 1}}>
      {item ? (
        <DetailContactComponent navigation={navigation} dataDetail={item} />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  contactItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 12,
  },
  contactInfo: {
    flex: 1,
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 4,
  },
  email: {
    fontSize: 14,
    color: '#888',
  },
});
export default DetailContact;
