import {useIsFocused} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';

import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import apiProvider from '../utils/service/apiProvider';
import HomeComponent from '../component/section/HomeComponent';

const Home = ({navigation}) => {
  const [item, setListContact] = useState('');
  const isFocused = useIsFocused();
  useEffect(() => {
    getDataContact();
  }, [isFocused]);
  const getDataContact = async () => {
    const response = await apiProvider.getDataContact();
    if (response) {
      return setListContact(response.data);
    }
  };
  const DeleteDataContact = async item => {
    const response = await apiProvider.DeleteDataContact(item.id, item);
    if (response) {
      getDataContact();
    }
  };

  return (
    <View style={{flex: 1}}>
      {item ? (
        <HomeComponent
          navigation={navigation}
          listContact={item}
          onDelete={value => DeleteDataContact(value)}
        />
      ) : null}
    </View>
  );
};

export default Home;
