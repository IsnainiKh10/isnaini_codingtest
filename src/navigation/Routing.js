import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {NavigationContainer} from '@react-navigation/native';
// import AuthNavigation from './AuthNavigation';
// import MyTabs from './BottomNavigation';
import Home from '../screen/Home';
import SplashScreen from '../component/section/SplashScreen';
import DetailContact from '../screen/DetailContact';
import EditContact from '../screen/EditContact';
const Stack = createStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false, tabBarHideOnKeyboard: true}}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="DetailContact" component={DetailContact} />
        <Stack.Screen name="EditContact" component={EditContact} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
