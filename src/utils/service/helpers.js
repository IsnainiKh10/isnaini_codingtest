import {Colors} from '../../styles';
import {Platform, ToastAndroid} from 'react-native';

export const queryParams = obj => {
  var str = [];
  for (var p in obj) {
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  }
  return str.join('&');
};

export const opacityColor = (colour, value) => {
  const opacity = Math.floor(0.1 * value * 255).toString(16);
  return colour + opacity;
};

export const showToastMessage = msg => {
  if (Platform.OS === 'android') {
    ToastAndroid.showWithGravityAndOffset(
      msg,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50,
    );
  } else {
    if (msg) {
      alert(msg);
    }
  }
};

export const resultColor = val => {
  const data = parseInt(val);
  if (!data) {
    return Colors.GREEN_MONEY;
  }
  if (data < 0) {
    return Colors.RED;
  } else {
    return Colors.GREEN_MONEY;
  }
};

/** function for change "case" to "Title Case" */
export const toTitleCase = string => {
  if (!string) {
    return '';
  }
  return string
    .split(' ')
    .map(word => word[0].toUpperCase() + word.slice(1).toLowerCase())
    .join(' ');
};

export const iconItem = val => {
  if (!val) {
    return 'C';
  }
  let iconText = 'C';
  const splitStr = val.split(' ');
  const string1 = splitStr[0].charAt(0).toUpperCase();
  if (splitStr[1]) {
    const string2 = splitStr[1].charAt(0).toUpperCase();
    iconText = string1 + string2;
  } else {
    iconText = string1;
  }
  return iconText;
};
